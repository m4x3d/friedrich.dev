import React, { FunctionComponent } from "react";
import { Anchor, Heading, Paragraph } from "grommet";
import { Layout } from "../components/Layout";
import styled from "styled-components";

const StyledHeading = styled(Heading)`
    margin: 2rem 0.5rem 0.5rem 0.5rem;
`;

const StyledParagraph = styled(Paragraph)`
    margin: 0.5rem;
    max-width: 100%;
`;

const StyledSource = styled(StyledParagraph)`
    margin-top: 2rem;
`;

export const Imprint: FunctionComponent = () => (
    <Layout>
      <Heading level="2" margin="0.5rem">Impressum</Heading>
      <Heading level="3" margin="0.5rem">Angaben gemäß § 5 TMG</Heading>
      <StyledParagraph>Max Friedrich</StyledParagraph>
      <StyledParagraph>Michaelisstraße 18c</StyledParagraph>
      <StyledParagraph>64293 Darmstadt</StyledParagraph>

      <StyledHeading level="3">Kontakt</StyledHeading>
      <StyledParagraph>Telefon: +49 (0) 174 9195538</StyledParagraph>
      <StyledParagraph>
        {'E-Mail: '}
        <Anchor href="mailto:max@friedrich.dev" label="max@friedrich.dev" weight="bold" />
      </StyledParagraph>

      <StyledHeading level="4">Haftung für Inhalte</StyledHeading>
      <StyledParagraph>
        Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den
        allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht,
        übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach Umständen zu
        forschen, die auf eine rechtswidrige Tätigkeit hinweisen.
      </StyledParagraph>
      <StyledParagraph>
        Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen
        Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der
        Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden
        Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.
      </StyledParagraph>

      <StyledHeading level="4">Haftung für Links</StyledHeading>
      <StyledParagraph>
        Unser Angebot enthält Links zu externen Websites Dritter, auf deren Inhalte wir keinen Einfluss haben.
        Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der
        verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten
        Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte
        waren zum Zeitpunkt der Verlinkung nicht erkennbar.
      </StyledParagraph>
      <StyledParagraph>
        Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer
        Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links
        umgehend entfernen.
      </StyledParagraph>

      <StyledHeading level="4">Urheberrecht</StyledHeading>
      <StyledParagraph>
        Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen
        Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der
        Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers.
        Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet.
      </StyledParagraph>
      <StyledParagraph>
        Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter
        beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine
        Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei
        Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.
      </StyledParagraph>

      <StyledSource>
        {'Quelle: '}
        <Anchor 
          href="https://www.e-recht24.de/impressum-generator.html" 
          label="https://www.e-recht24.de/impressum-generator.html" 
          weight="bold"
        />
      </StyledSource>
    </Layout>
)

export default Imprint