import React, { FunctionComponent } from "react"
import { Layout } from "../components/Layout"
import { Anchor, Heading, Paragraph } from "grommet"
import { GrGithub, GrMedium, GrLinkedinOption, GrTwitter } from "react-icons/gr"
import { FaGitlab, FaXing } from "react-icons/fa"
import styled from "styled-components"
import { SvgIcon } from "../components/SvgIcon"

const StyledAnchor = styled(Anchor)`
  padding: 0.5rem;
`;

const IconBar = styled.div`
  margin-top: 3rem;
  display: flex;
  justify-content: space-evenly;
`;

const IndexPage: FunctionComponent = () => {
  return (
    <Layout>
      <Heading level="2" margin="0.5rem">{"Hey there!"}</Heading>
      <Paragraph margin="0.5rem">{"My name is Max. I'm a web developer at "}
        <Anchor href="https://www.incloud.de/" weight="bold" label="INCLOUD" />.
      </Paragraph>
      <IconBar>
        <StyledAnchor href="https://gitlab.com/m4xd" label={<SvgIcon size="large" icon={<FaGitlab fill="inherit" />} />} />
        <StyledAnchor href="https://github.com/m4xd7" label={<SvgIcon size="large" icon={<GrGithub fill="inherit" />} />} />
        <StyledAnchor href="https://medium.com/@m4xd" label={<SvgIcon size="large" icon={<GrMedium fill="inherit" />} />} />        
        <StyledAnchor href="https://www.xing.com/profile/Max_Friedrich46/cv" label={<SvgIcon size="large" icon={<FaXing fill="inherit" />} />} />
        <StyledAnchor href="https://www.linkedin.com/in/max-friedrich-119852206/" label={<SvgIcon size="large" icon={<GrLinkedinOption fill="inherit" />} />} />
        <StyledAnchor href="https://twitter.com/m4xd_7" label={<SvgIcon size="large" icon={<GrTwitter fill="inherit" />} />} />
      </IconBar>
    </Layout>
  )
}

export default IndexPage
