import { ThemeType } from "grommet";

export const colors = {
  "lightGrey": "#f8f8f8",
  "darkGrey": "#161618",
  "creme": "#eae7dc"
}

export const theme: ThemeType = { 
  global: {
    focus: {
      outline: {
        size: "0"
      }
    },
    font: {
      family: "Open Sans"
    },
    colors: { 
      text: { 
        "dark": colors.lightGrey, 
        "light": colors.darkGrey
      },
      background: {
        "dark": colors.darkGrey,
        "light": colors.creme, 
      }
    }
  },
  anchor: {
    color: {
    "dark": colors.lightGrey, 
    "light": colors.darkGrey 
    },
    fontWeight: 400
  }
}