import React, { FunctionComponent } from "react"
import { FiMoon, FiSun } from "react-icons/fi"
import { Anchor, Button } from "grommet"
import styled from "styled-components";
import { SvgIcon } from "./SvgIcon";

const StyledHeader = styled.div`
  margin: auto;
  padding: 1rem;
  max-width: 55rem;
  top: 0;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

type HeaderProps = {
  themeMode: "light" |"dark",
  setThemeMode: React.Dispatch<React.SetStateAction<"light" | "dark">>
}

export const Header: FunctionComponent<HeaderProps> = ({ themeMode, setThemeMode }) => (
  <StyledHeader >
    <Anchor href="/" label="Max Friedrich" />
    {themeMode === "light" ? 
      <Button onClick={() => setThemeMode("dark")}>
        <SvgIcon icon={<FiMoon size="2rem" stroke="inherit" />} />
      </Button> 
      : 
      <Button onClick={() => setThemeMode("light")}>
        <SvgIcon icon={<FiSun size="2rem" stroke="inherit" />} />
      </Button>
    }
  </StyledHeader>
)