import React, { FunctionComponent } from "react";
import { Blank } from "grommet-icons"

enum IconSize {
  small = "24",
  medium = "32",
  large = "48"
}

interface SvgIconProps {
  icon: JSX.Element
  size?: "small" | "medium" | "large"
}

export const SvgIcon: FunctionComponent<SvgIconProps> = ({ icon, size = "medium" }) => (
  <Blank size={IconSize[size]}>
    {icon}
  </Blank>
)