import React from "react"
import { FunctionComponent } from "react";
import { Anchor, Text } from "grommet"
import { GrGatsbyjs, GrGrommet } from "react-icons/gr";
import styled from "styled-components";
import { SvgIcon } from "./SvgIcon";
import { colors } from "../utils/theme";

const StyledFooter = styled.footer`
  padding: 1rem 0;
  width: 100%;
  bottom: 0;
  position: fixed;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const FooterFlex = styled.div`
  margin-right: 0.5rem;
  display: flex;
  align-items: center;
`;

const StyledAnchor = styled(Anchor)`
  margin-top: 0.25rem;
`;

const StyledDelimiter = styled(Text)`
  margin: 0 0.5rem;
`;

interface FooterProps {
  themeMode: "light" | "dark"
}

export const Footer: FunctionComponent<FooterProps> = ({ themeMode }) => (
  <StyledFooter style={{backgroundColor: themeMode === "light" ? colors.creme : colors.darkGrey }}>
    <FooterFlex>
      <Text margin="0.5rem">
          {'Made with'}
      </Text>
      <StyledAnchor href='https://www.gatsbyjs.com/' label={<SvgIcon size="small" icon={<GrGatsbyjs size="1rem" fill="inherit" />} />} />
      <Text margin="0.5rem">{'and'}</Text>
      <StyledAnchor href='https://grommet.io/' label={<SvgIcon size="small" icon={<GrGrommet size="1rem" fill="inherit" />} />} />
    </FooterFlex>
    <StyledDelimiter>
      {'|'}
    </StyledDelimiter>
    <Text margin="0.5rem">
      {`${new Date().getFullYear()} - Max Friedrich`}
    </Text>
    <StyledDelimiter>
      {'|'}
    </StyledDelimiter>
    <Anchor margin="0.5rem" href='/imprint' label="Imprint" />
  </StyledFooter>
)