import React, { FunctionComponent, useState } from "react"
import { Grommet } from "grommet"
import { GlobalStyle } from "../utils/globalStyle";
import { theme } from "../utils/theme";
import { Header } from "./Header";
import { Footer } from "./Footer";
import styled from "styled-components";
import "fontsource-open-sans"

const StyledGrommet = styled(Grommet)`
  min-height: 100vh;
`;

const StyledMain = styled.main`
  margin: 15rem auto 0 15rem;
  padding-bottom: 15rem;
  max-width: 55rem;
`;

export const Layout: FunctionComponent = ({ children }) => {
  const browserDarkMode: boolean = window.matchMedia('(prefers-color-scheme: dark)').matches;
  const [themeMode, setThemeMode] = useState<"light" | "dark">(browserDarkMode ? "dark" : "light")

  return (
    <StyledGrommet theme={theme} themeMode={themeMode}>
      <GlobalStyle />
      <title>{'Max Friedrich'}</title>
      <Header themeMode={themeMode} setThemeMode={setThemeMode} />
      <StyledMain>
        {children}
      </StyledMain>
      <Footer themeMode={themeMode} />
    </StyledGrommet>
  )
}